# -*- coding: utf-8 -*-
# @project : chinaNature
# @File    : add_crawl.py
# @Author  : Huang
# @Date    : 2020-03-23 9:58
# Software : PyCharm
# version： Python 3.6.7


# 增量抓取成交公示公告信息；
#
# 每次抓取先获取首页的日期，和之前抓取保存的记录时间对比，如果当前获取的时间大于记录时间则获取，否则不抓取

# 如果首页的时间大于记录的时间抓取，继续获取第二页的时间依次对比，直到获取到的时间小于等于记录的时间则停止抓取
import json
import os

from conf import result_url
from htmldownload import get
from schedule import parse_pages_time, start, parse_urls
from htmlsave import save_time

import logging
import sys

logger = logging.getLogger(__file__)
logger.setLevel(level=logging.DEBUG)

# StreamHandler
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(level=logging.DEBUG)
logger.addHandler(stream_handler)

# FileHandler
file_handler = logging.FileHandler(f'Log/add_crawl.log')
file_handler.setLevel(level=logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


class CustomError(Exception):
    def __init__(self, ErrorInfo):
        super().__init__(self)
        self.errorinfo = ErrorInfo

    def __str__(self):
        return self.errorinfo


class AddCrawl:     
    @staticmethod
    def get_crawl_signal(result_type, current_time):
        """
        对比每个类型记录的时间，当前页抓取的时间大于则返回True, 否则False
        :param result_type: 成交公示的类型
        :param current_time: 上次抓取获取记录的时间
        :return: True or False
        """
        try:
            with open(f'CurrentTime/{result_type}/current.txt', 'r', encoding='utf-8') as f:
                data = json.loads(f.read())
                record_time = data.get("current_time")
        except CustomError as e:
            logger.error(f'获取记录时间错误: {e.args}')
            raise CustomError('获取记录时间错误')
        except FileNotFoundError:
            logger.error(f'CurrentTime/{result_type}/current.txt 文件未找到')
        else:
            if current_time > record_time:
                return True
            else:
                return False

    def start_crawl(self, result_type):  # TODO  页面只更新几条公告(根据时间去掉已重复抓取的公告？)or 都抓然后去重
        """
        开始增量抓取
        """
        # 获取每个类型的首页的时间
        logger.debug(f'============开始增量抓取============')
        pre = "http://landchina.mnr.gov.cn/land/cjgs/{}/index_{}.htm"
        i = 0
        while True:
            if i == 0:
                crawl_url = result_url+result_type
            else:
                crawl_url = pre.format(result_type, i)
            res = get(url=crawl_url)
            pages, current_time = parse_pages_time(data=res)
            signal = self.get_crawl_signal(result_type=result_type, current_time=current_time)
            if signal is True:
                # 更新记录时间
                save_time(result_type=result_type, info={"result_type": result_type, "current_time": current_time})
                logger.info(f'当前{result_type}新增第{i}页面的时间{current_time}大于记录时间, 开始获取新增页面')
                page_anno_urls = parse_urls(url=crawl_url, data=res, result_type=result_type)
                for j, link in enumerate(page_anno_urls):
                    start(result_type=result_type, url=link, index='', addition=True)
                    logger.debug(f'当前{result_type}新增第{i}页的第{j+1}个公告获取完成')
                i += 1
            elif signal is False:
                logger.info(f'当前{result_type}获取时间{current_time}小于等于记录时间, 不再抓取')
                break


if __name__ == '__main__':
    add = AddCrawl()
    for each in ['xycr', 'hbgd', 'zbcr', 'gpcr', 'pmcr']:
        logger.debug(f'开始新增抓取{each}公告')
        add.start_crawl(result_type=each)
        logger.debug(f'新增抓取{each}完成')
    logger.debug(f'全部类型新增抓取完成')

