# -*- coding: utf-8 -*-
# @project : chinaNature
# @File    : handle_table.py
# @Author  : Huang
# @Date    : 2020/3/23 14:25
# Software : PyCharm
# version： Python 3.6.7
import csv
import os
import sys
import logging

from lxml import etree


logger = logging.getLogger(__file__)
logger.setLevel(level=logging.DEBUG)

# StreamHandler
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(level=logging.DEBUG)
logger.addHandler(stream_handler)

# FileHandler
file_handler = logging.FileHandler(f'Log/handle_table.log')
file_handler.setLevel(level=logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def get_files(file_path):
    """
    获取需要解析表格的html文件
    :param file_path: 需要解析表格的html文件路径; 文件夹名称
    :return: 所有的html文件绝对路径
    """
    files_list = []
    if file_path is not None:
        for root, dirs, files in (os.walk(file_path)):
            for file in files:
                files_list.append(os.path.join(root, file))
    # logger.debug(f'{files_list}')
    return files_list


def del_key(info_list):  # TODO  主要用途已经有了；面积也等于宗地总面积；暂时不要了
    """
    {'用途名称': '面积', '工业用地': '71618.74'}
    {'用途名称': '工业用地',  '面积': '71618.74'}
    将通途和面积的值互换
    :param info_list:handle_table解析出来的表格list
    :return: 互换之后的dict
    """
    print(f"清洗info_dict")
    new_info_list = []
    for info_dict in info_list:
        for d_key in ['用地', '名称', '无']:
            keys = list(info_dict.keys())
            for key in keys:
                if d_key in key:
                    del info_dict[key]
        new_info_list.append(info_dict)
    return new_info_list


def parse_table(file, table_xpath=''):
    """
    解析表格
    :param file: 表格路径
    :param table_xpath: 表格的xpath路径
    :return: 解析之后的表格信息；list
    """
    try:
        with open(file, 'r', encoding='utf-8') as f:
            data = f.read()
    except Exception:
        with open(file, 'r', encoding='gbk') as f:
            data = f.read()
        logger.error(f'打开{file}文件错误', exc_info=True)
    html = etree.HTML(data)
    tables = html.xpath(table_xpath)
    # tables = html.xpath("//*[@class='gu-art-con']/table//table")
    info_list = []
    # 全部是处理成横表
    for table in tables:
        info_dict = {}
        trs = table.xpath(".//tr")
        for tr in trs:
            td_eles = tr.xpath("./td/text()")
            td_text_list = []  # 横表里的每个tr下所有的td文本
            # print(f'每个横表下的td长度：{len(td_eles)}')
            for item in td_eles:  # 用途名称下面是空的情况
                # print(item)
                if item is None:
                    item = '无'
                else:
                    item = item.replace(' ', '').replace('\xa0', '').replace('\n', '').strip()
                td_text_list.append(item)
            # td个数是偶数的情况下；让第奇数个的td的值作为键，偶数个的td的值作为值
            if len(td_eles) % 2 == 0:
                for i in range(0, len(td_text_list), 2):
                    info_dict[td_text_list[i]] = td_text_list[i+1]
            # 奇数情况 gyyd_htmls/第100页/t20191116_7311603.htm备案号后面有td但是没有值；最后的第奇数个td的值作为键，值留空
            else:
                for i in range(0, len(td_text_list), 2):
                    if i+1 < len(td_text_list):
                        info_dict[td_text_list[i]] = td_text_list[i+1]
                        info_dict[td_text_list[len(td_text_list)-1]] = ''
        info_list.append(info_dict)
    # print(info_list)
    return info_list


def save(anno_tpye, file_name, info):  # TODO 每个公告多个表格放一个表还是多个表？
    """
    保存到本地csv
    :param anno_tpye: 抓取类型的字母缩写
    :param file_name: 文件名称
    :param info: 解析table的dict_info
    """
    try:
        table_path = f'./ParseTable/{anno_tpye}'
        if not os.path.exists(table_path):
            os.makedirs(table_path)
        with open(f'{table_path}/{file_name}.csv', 'a', encoding='utf_8_sig', newline='') as f:
            header = list(info.keys())
            csv_writer = csv.DictWriter(f, fieldnames=header)
            with open(f'{table_path}/{file_name}.csv', 'r', encoding='utf_8_sig', newline='') as fe:
                reader = csv.reader(fe)
                if not [item for item in reader]:
                    csv_writer.writeheader()
                    csv_writer.writerow(info)
                else:
                    csv_writer.writerow(info)
    except Exception:
        logger.error(f'{file_name}存入字段信息错误: ', exc_info=True)


def start(file_path):  # TODO 公告里的表格是空的情况
    """
    获取文件并解析表格并保存到本地
    :param file_path: html文件路径; 绝对路径：'E:\\My\\Test\\chinaNature\\output\\zbcr\\第121页'
    """
    files = get_files(file_path)
    for file in files:
        """
        E:\\My\\Test\\chinaNature\\output\\zbcr\第106页\t20190805_7210977.htm
        """
        logger.debug(f'开始解析{file}')
        result_type = file.split('\\')[5]  # zbcr
        logger.debug(f'{result_type}')
        filename = file.split("\\")[-1][:-4]  # t20190625_7169096
        # logger.debug(f'{filename}')
        infos = parse_table(file, table_xpath="//*[@class='gu-art-con']/table//table")
        new_infos = del_key(infos)
        # logger.debug(f'{file}解析到的表格信息{new_infos}')
        # logger.debug(f'{file}解析完成')
        for info in new_infos:
            # logger.debug(f'{file}解析出来的字典信息{info}')
            save(anno_tpye=result_type, file_name=filename, info=info)
        logger.debug(f'{file}解析并保存完毕')
    logger.debug(f'================{len(files)}全部解析保存完毕==================')


if __name__ == '__main__':
    # print(get_files(r'E:\My\爬虫抓取存放\中国自然资源部\gyyd_htmls'))
    start(r'E:\My\Test\chinaNature\output\zbcr\第121页')
    # start('syyd_htmls')
    # infos = handle_table(r'gyyd_htmls\第100页\t20191116_7311603.htm')
    # for info in del_key(infos):
    #     print(info)
    # # print(handle_table(r'gyyd_htmls\第100页\t20191116_7311603.htm'))
    # print(handle_table('t20191116_7311603.htm'))