# -*- coding: utf-8 -*-
# @project : chinaNature
# @File    : htmlparse.py
# @Author  : Huang
# @Date    : 2020-03-19 9:03
# Software : PyCharm
# version： Python 3.6.7
import datetime
import os
import re

from lxml import etree
from conf import result_url
import logging
import sys

logger = logging.getLogger(__file__)
logger.setLevel(level=logging.DEBUG)

# StreamHandler
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(level=logging.DEBUG)
logger.addHandler(stream_handler)

# FileHandler
file_handler = logging.FileHandler(f'Log/parser.log')
file_handler.setLevel(level=logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def parse_pages_time(data):
    """
    获取最大页码数和首页的公示的时间
    :param data: 首页网页源码
    :return: 最大页码数
    """
    # time_now = datetime.datetime.now().strftime("%Y.%m.%d")
    if data is not None:
        try:
            html = etree.HTML(data)
            pages_ele = html.xpath("//div[@class='gu-page gu-page1']/script/text()")
            pages = re.findall(r'.* var countPage = (\d+)//共多少页', pages_ele[0], re.S) if pages_ele is not None else ''
            pages = pages[0] if pages is not None else 0
            # time
            current_time = html.xpath("//ul[@class='gu-iconList']//span/text()")
            current_time = max(current_time) if current_time is not None else ''
            if pages != 0:
                return int(pages), current_time
        except Exception:
            logger.error(f'获取页码数错误', exc_info=True)
            return 0, 0


def parse_urls(url, data, result_type):
    """
    获取当前页所有公告的链接
    :param url: url
    :param data: 网页源码
    :param result_type: 成交公示类型，构造url
    :return: 当前页所有公告的链接
    """
    if data is not None:
        try:
            html = etree.HTML(data)
            links_eles = html.xpath("//div[@class='gu-ky-list']/ul/li/a/@href")  # './202002/t20200213_7394498.htm'
            links = [result_url + result_type + url[1:] for url in links_eles if len(links_eles) > 0]
            # logger.debug(f'当前页的所有公告链接： {links}')
            return links
        except Exception:
            logger.error(f'获取{result_type}公告{url}链接错误', exc_info=True)
            return


def get_files(file_path):
    """
    获取需要解析表格的html文件
    :param file_path: 需要解析表格的html文件路径; 文件夹名称
    :return: 所有的html文件绝对路径
    """
    files_list = []
    if file_path is not None:
        for root, dirs, files in (os.walk(file_path)):
            for file in files:
                files_list.append(os.path.join(root, file))
    # logger.debug(f'{files_list}')
    return files_list


def parse_table(file, table_xpath=''):
    """
    解析表格
    :param file: 表格路径
    :param table_xpath: 表格的xpath路径
    :return: 解析之后的表格信息；list
    """
    try:
        with open(file, 'r', encoding='utf-8') as f:
            data = f.read()
    except Exception:
        with open(file, 'r', encoding='gbk') as f:
            data = f.read()
        logger.error(f'打开{file}文件错误', exc_info=True)
    html = etree.HTML(data)
    tables = html.xpath(table_xpath)
    # tables = html.xpath("//*[@class='gu-art-con']/table//table")
    info_list = []
    # 全部是横表
    for table in tables:
        info_dict = {}
        trs = table.xpath(".//tr")
        logger.debug(f'当前表格含有{len(trs)}个tr')
        for tr in trs:
            td_eles = tr.xpath("./td/text()")
            td_text_list = []  # 横表里的每个tr下所有的td文本
            # print(f'每个横表下的td长度：{len(td_eles)}')
            for item in td_eles:  # 用途名称下面是空的情况
                # print(item)
                if item is None:
                    item = '无'
                else:
                    item = item.replace(' ', '').replace('\xa0', '').replace('\n', '').strip()
                td_text_list.append(item)
            # td个数是偶数的情况下；让第奇数个的td的值作为键，偶数个的td的值作为值
            if len(td_eles) % 2 == 0:
                for i in range(0, len(td_text_list), 2):
                    info_dict[td_text_list[i]] = td_text_list[i+1]
            # 奇数情况 gyyd_htmls/第100页/t20191116_7311603.htm备案号后面有td但是没有值；最后的第奇数个td的值作为键，值留空
            else:
                for i in range(0, len(td_text_list), 2):
                    if i+1 < len(td_text_list):
                        info_dict[td_text_list[i]] = td_text_list[i+1]
                        info_dict[td_text_list[len(td_text_list)-1]] = ''
        info_list.append(info_dict)
    # print(info_list)
    return info_list


def del_key(info_list):  # TODO  主要用途已经有了；面积也等于宗地总面积；暂时不要了
    """
    {'用途名称': '面积', '工业用地': '71618.74'}
    {'用途名称': '工业用地',  '面积': '71618.74'}
    将通途和面积的值互换
    :param info_list:handle_table解析出来的表格list
    :return: 互换之后的dict
    """
    print(f"清洗info_dict")
    new_info_list = []
    for info_dict in info_list:
        for d_key in ['用地', '名称', '无']:
            keys = list(info_dict.keys())
            for key in keys:
                if d_key in key:
                    del info_dict[key]
        new_info_list.append(info_dict)
    return new_info_list


if __name__ == '__main__':
    pass
    # files = (get_files(r'E:\My\Test\chinaNature\output'))
    # for file in files:
    #     print(file)
    #     infos = parse_table(file=file, table_xpath="//*[@class='gu-art-con']/table//table")
    #     print(del_key(infos))
    #     break

