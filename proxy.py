# -*- coding: utf-8 -*-
# @project : shop_58
# @File    : proxy.py
# @Author  : Huang
# @Date    : 2020-03-02 14:37
# Software : PyCharm
# version： Python 3.6.7
import datetime
import random
import time

import requests
from requests.exceptions import RequestException, ConnectTimeout, ReadTimeout, ConnectionError
from conf import USER_AGENT_LIST

import logging
import sys

logger = logging.getLogger(__file__)
logger.setLevel(level=logging.DEBUG)

# StreamHandler
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(level=logging.DEBUG)
logger.addHandler(stream_handler)

# FileHandler
file_handler = logging.FileHandler(f'checkproxy_valid.log')
file_handler.setLevel(level=logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


class Proxy:
    def __init__(self):
        # http; 随机端口数; 不限运营商; 当日去重
        self.direct_api = 'http://webapi.http.zhimacangku.com/getip?num={}&type=2&pro=&city=0&yys=0&port=1&pack=86890&ts=1&ys=1&cs=1&lb=1&sb=0&pb=45&mr=2&regions='
        self.suidao_api = 'http://http.tiqu.alicdns.com/getip3?num={}&type=2&pro=&city=0&yys=0&port=1&pack=86890&ts=1&ys=1&cs=1&lb=1&sb=0&pb=45&mr=2&regions='
        self.dx_api = "http://http.tiqu.alicdns.com/getip3?num={}&type=2&pro=&city=0&yys=0&port=1&pack=86890&ts=1&ys=1&cs=1&lb=1&sb=0&pb=45&mr=2&regions=&gm=4"

    def get_proxy(self, req_count, api):
        """
        获取芝麻代理并判断是否过期
        :return: proxies
        """
        ips = []
        try:
            time.sleep(1)
            response = requests.get(url=api.format(req_count))
            if response.status_code == 200:
                data = response.json().get('data')
                for each in data:
                    expire_time = each.get('expire_time')
                    proxies = {'http': 'http://' + each.get('ip') + ':' + str(each.get('port'))}
                    # proxies = 'https:' + each.get('ip') + ':' + str(each.get('port'))
                    ips.append({'proxies': proxies, 'expire_time': expire_time})
                # print(f'获取到的ip: {ips}')
                return ips
        except Exception as e:
            self.get_proxy(req_count, api)

    def check_proxy(self, check_url, proxies=None):
        """
        检测代理
        :param check_url:检测网站
        :param proxies:芝麻代理
        :return:proxy
        """
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'User-Agent': random.choice(USER_AGENT_LIST),
            'Accept-Encoding': 'gzip, deflate',
            'Host': 'landchina.mnr.gov.cn',
            'Connection': 'close',
        }
        try:
            time.sleep(random.randint(2, 4))
            response = requests.get(url=check_url, headers=headers, timeout=2, proxies=proxies)
            if response.status_code == 200:
                return True
        except ReadTimeout as e:
            logger.debug(f'检测代理: ReadTimeout异常, {e.args}')
            return False
        except ConnectTimeout as e:
            logger.debug(f'检测代理: ConnectTimeout异常, {e.args}')
            return False
        except ConnectionError as e:
            logger.debug(f'检测代理: ConnectionError异常, {e.args}')
            return False
        except RequestException as e:
            logger.debug(f'检测代理: RequestException异常, {e.args}')
            return False

    def test_proxy(self, api):  # TODO 隧道ip50个成功43个；独享33个；直连35个
        failed_count = 0
        succ_count = 0
        ips = self.get_proxy(req_count=50, api=api)
        for index, pro in enumerate(ips):
            result = self.check_proxy(check_url='http://landchina.mnr.gov.cn/land/cjgs/', proxies=pro.get('proxies'))
            if result is True:
                print(f'第{index+1}个ip检测成功')
                succ_count += 1
            else:
                print(f'第{index+1}个ip检测失败')
                failed_count += 1
        print(f'总实验次数：{len(ips)}, {api}成功数：{succ_count}')


if __name__ == '__main__':
    p = Proxy()
    # p.test_proxy(api=p.suidao_api)
    # p.test_proxy(api=p.dx_api)
    p.test_proxy(api=p.direct_api)