# -*- coding: utf-8 -*-
# @project : chinaNature
# @File    : schedule.py
# @Author  : Huang
# @Date    : 2020-03-19 9:39
# Software : PyCharm
# version： Python 3.6.7
# import datetime
# import json

# import requests
# from urllib.parse import urljoin

from htmldownload import get, get_all_pages_urls
from htmlsave import save_html, save_time
from htmlparse import parse_pages_time, parse_urls
# from proxy import Proxy
from conf import result_url, get_proxies_api

import logging
import sys

logger = logging.getLogger(__file__)
logger.setLevel(level=logging.DEBUG)

# StreamHandler
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(level=logging.DEBUG)
logger.addHandler(stream_handler)

# FileHandler
file_handler = logging.FileHandler(f'Log/scheduler.log')
file_handler.setLevel(level=logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def scheduler2(result_type):
    """
    调度;不使用代理
    :param result_type: 成交公示类型
    """
    logger.debug(f'============开始抓取==========')
    res = get(url=result_url+result_type)  # 第一页
    first_page_urls = parse_urls(url=result_url+result_type, data=res, result_type=result_type)
    logger.debug(f'获取到{result_type}首页上有{len(first_page_urls)}个公告')
    if first_page_urls is not None:
        for j, link in enumerate(first_page_urls):
            start(result_type=result_type, url=link, index=1)
            logger.debug(f'{result_type}首页上第{j+1}个公告获取完成')
    logger.debug(f'{result_type}第一页所有的公告信息获取完毕')
    pages, current_time = parse_pages_time(res)  # TODO NoneType is not itreable
    logger.info(f'当前{result_type}类型的最大页码数: {pages}; 最大时间: {current_time}')
    save_time(result_type=result_type, info={"result_type": result_type, "current_time": current_time})
    pages_urls = get_all_pages_urls(result_type=result_type, pages=pages)
    if len(pages_urls) > 0:
        for i, link in enumerate(pages_urls):
            logger.debug(f'开始请求{result_type}的第{i+2}页')
            index = link.split('_')[-1].split('.')[0]  # 当前第几页
            data = get(url=link)
            page_anno_urls = parse_urls(url=link, data=data, result_type=result_type)
            for x, single_url in enumerate(page_anno_urls):
                logger.debug(f'当前{i+2}页下的第{x+1}个公告获取完成')
                start(result_type=result_type, url=single_url, index=index)
            logger.debug(f'{result_type}的第{i+2}页所有的公告信息获取完毕')
        logger.debug(f'=========={result_type}当前所有公告信息获取完毕=============')


def start(result_type, url, index, proxies=None, addition=False):
    """
    请求并保存当前页公告信息的封装
    :param result_type: 成交公示的类型
    :param url: url
    :param index: 当前页
    :param proxies: proxies
    :param addition: 是否新增抓取
    """
    file_name = url.split('/')[-1]
    res = get(url=url, proxies=proxies)
    save_html(result_type=result_type, index=index, data=res, file_name=file_name, addition=addition)


# def scheduler(result_type):
#     """
#     调度；使用代理
#     :param result_type: 成交公示类型
#     """
#     # now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
#     # proxies, expire_time = get_check_proxy()
#     proxies, expire_time = get_use_proxy()
#     print(f'获取到的代理： {proxies}')
#     res = get_response(url=urljoin(result_url, result_type), proxies=proxies)  # 第一页
#     first_page_urls = parse_urls(url=urljoin(result_url, result_type), data=res, result_type=result_type)  # 当前页的公告url
#     if first_page_urls is not None:
#         for j, link in enumerate(first_page_urls):
#             start(result_type=result_type, url=link, index=1)
#             logger.debug(f'{result_type}首页上第{j+1}个公告获取完成')
#         logger.debug(f'{result_type}第一页所有的公告信息获取完毕')
#     pages, current_time = parse_pages_time(res)  # 获取当前最大时间和最大页码数; TODO NoneType is not itreable
#     logger.info(f'当前{result_type}类型的最大页码数: {pages}; 最大时间: {current_time}')
#     save_time(result_type=result_type, info={"result_type": result_type, "current_time": current_time})
#     pages_urls = get_all_pages_urls(result_type=result_type, pages=pages)  # 获取所有分页urls
#     if len(pages_urls) > 0:
#         for i, link in enumerate(pages_urls):
#             logger.debug(f'开始请求{result_type}的第{i+2}页')
#             now1_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
#             while now1_time >= expire_time:
#                 logger.debug(f'当前{proxies}已过期更换下一个')
#                 # proxies1, expire_time1 = get_check_proxy()
#                 proxies1, expire_time1 = get_use_proxy()
#                 logger.debug(f'获取到新的代理: {proxies1}')
#                 expire_time = expire_time1
#                 proxies = proxies1
#             else:
#                 index = link.split('_')[-1].split('.')[0]  # 当前第几页
#                 data = get_response(url=link, proxies=proxies)
#                 page_anno_urls = parse_urls(url=link, data=data, result_type=result_type)
#                 for single_url in page_anno_urls:
#                     now2_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
#                     while now2_time >= expire_time:
#                         logger.debug(f'当前{proxies}已过期更换下一个')
#                         # proxies2, expire_time1 = get_check_proxy()
#                         proxies2, expire_time1 = get_use_proxy()
#                         logger.debug(f'获取到新的代理: {proxies2}')
#                         expire_time = expire_time1
#                         proxies = proxies2
#                     else:
#                         start(result_type=result_type, url=single_url, proxies=proxies, index=index)
#             logger.debug(f'{result_type}的第{i+2}页所有的公告信息获取完毕')
#         logger.debug(f'=========={result_type}当前所有公告信息获取完毕=============')


# def get_check_proxy():
#     """
#     获取并检测代理，如果能正常访问result_url则返回该代理
#     :return:
#     """
#     response = requests.get(url=get_proxies_api)
#     """
#     {"expire_time":"2020-03-19 17:24:13","proxies":{"http":"http://58.218.200.248:3193"}}
#     """
#     data = json.loads(response.text)
#     proxies = data.get("proxies")
#     expire_time = data.get("expire_time")
#     pro = Proxy()
#     result = pro.check_proxy(check_url=result_url, proxies=proxies)
#     while True:
#         if result is True:
#             return proxies, expire_time
#         else:
#             logger.debug(f'本次从池子中获取的代理{proxies}检测无效, 获取下一个代理')
#             get_check_proxy()
#
#
# def get_use_proxy():
#     """
#     请求隧道api并检测代理有效性直到代理有用
#     :return: 代理
#     """
#     pro = Proxy()
#     info = pro.get_proxy(req_count=1, api=pro.direct_api)
#     proxies = info[0].get("proxies") if info is not None else None
#     expire_time = info[0].get("expire_time") if info is not None else None
#     """
#     {'proxies': proxies, 'expire_time': expire_time}
#     """
#     result = pro.check_proxy(check_url=result_url, proxies=proxies)
#     while True:
#         if result is True:
#             return proxies, expire_time
#         else:
#             # logger.debug(f'本次获取的代理: {proxies}检测无效继续请求api获取')
#             get_use_proxy()
#
#
# def get_response(url, proxies):
#     """
#     处理代理的异常，直到请求返回response
#     :param url: url
#     :param proxies: proxies
#     :return: response
#     """
#     res = get(url, proxies)
#     while True:
#         if res is not None:
#             return res
#         else:
#             logger.debug(f'本次请求异常, 切换ip继续请求')
#             new_proxies, expire_time = get_check_proxy()
#             logger.debug(f'切换代理：{new_proxies}')
#             get_response(url, new_proxies)