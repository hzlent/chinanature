# -*- coding: utf-8 -*-
# @project : chinaNature
# @File    : htmlsave.py
# @Author  : Huang
# @Date    : 2020-03-19 9:03
# Software : PyCharm
# version： Python 3.6.7
import json
import os

import logging
import sys

logger = logging.getLogger(__file__)
logger.setLevel(level=logging.DEBUG)

# StreamHandler
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(level=logging.DEBUG)
logger.addHandler(stream_handler)

# FileHandler
file_handler = logging.FileHandler(f'Log/saver.log')
file_handler.setLevel(level=logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def save_time(info, result_type):
    """
    记录当前类型的最大时间
    :param info: # {"result_type": "xycr", "current_time": ["2020.03.17", ]}
    :param result_type: 成交公示的类型
    """
    file_path = f'CurrentTime/{result_type}'
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    try:
        with open(f'{file_path}/current.txt', 'w', encoding='utf-8') as f:
            f.write(json.dumps(info, ensure_ascii=False) + '\n')
    except Exception as e:
        logger.error(f'记录更新时间错误', exc_info=True)


def save_html(result_type, index, file_name, data, addition=False):
    """
    保存网页源码
    :param result_type: 成交公示的类型
    :param index: 当前第几页
    :param file_name: file_name
    :param data: 网页源码
    :param addition: 是否增量抓取存储
    """
    if data is not None:
        if addition is False:
            output_path = f'./output/{result_type}/'
            full_file_path = os.path.join(output_path, f'第{index}页')
            if not os.path.exists(full_file_path):
                os.makedirs(full_file_path)
            try:
                with open(f'{full_file_path}/{file_name}', 'w', encoding='utf-8') as f:
                    f.write(data)
            except Exception:
                logger.error(f'保存{file_name}网页源码错误', exc_info=True)
        elif addition is True:
            add_file_path = f'./output/addition/{result_type}/'
            if not os.path.exists(add_file_path):
                os.makedirs(add_file_path)
            try:
                with open(f'{add_file_path}/{file_name}', 'w', encoding='utf-8') as f:
                    f.write(data)
            except Exception:
                logger.error(f'保存{file_name}网页源码错误', exc_info=True)


if __name__ == '__main__':
    # os.makedirs('a/b/c')
    save_time(info='', result_type='gptest')