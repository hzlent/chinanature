# -*- coding: utf-8 -*-
# @project : chinaNature
# @File    : main.py
# @Author  : Huang
# @Date    : 2020-03-18 18:11
# Software : PyCharm
# version： Python 3.6.7
import json
import os
import time
# from multiprocessing import Process, Pool
from schedule import scheduler2
# from proxy import Proxy
# import requests
# from conf import get_proxies_api, result_url


def start(result_type):
    log_file_path = 'Log'
    if not os.path.exists(log_file_path):
        os.mkdir(log_file_path)
    scheduler2(result_type)


if __name__ == '__main__':
    stime = time.time()
    # process1 = Process(target=start, args=('xycr', ))
    # process1.start()
    # process1.join()

    # pool = Pool(5)
    # pool.map(start, ['xycr', 'hbgd', 'zbcr', 'gpcr', 'pmcr'])
    # pool.close()
    # pool.join()
    for each in ['xycr', 'hbgd', 'zbcr', 'gpcr', 'pmcr']:
        start(result_type=each)
    ttime = time.time()
    total = ttime-stime
    print(f'总用时:{total}秒')