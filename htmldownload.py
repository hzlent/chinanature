# -*- coding: utf-8 -*-
# @project : chinaNature
# @File    : htmldownload.py
# @Author  : Huang
# @Date    : 2020-03-19 9:03
# Software : PyCharm
# version： Python 3.6.7

import requests
from requests.exceptions import ReadTimeout, ConnectTimeout, ConnectionError, RequestException
from conf import USER_AGENT_LIST, max_time, min_time, result_url
import logging
import sys
import time
import random
import socket
# from proxy import Proxy
# from main import get_check_proxy


logger = logging.getLogger(__file__)
logger.setLevel(level=logging.DEBUG)

# StreamHandler
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(level=logging.DEBUG)
logger.addHandler(stream_handler)

# FileHandler
file_handler = logging.FileHandler(f'Log/downloader.log')
file_handler.setLevel(level=logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def get(url, proxies=None):
    """
    发请求获取响应
    :param url: url
    :param proxies: proxies
    :return: 响应
    """
    time.sleep(random.randint(min_time, max_time))
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Encoding': 'gzip, deflate',
        'Host': 'landchina.mnr.gov.cn',
        'User-Agent': random.choice(USER_AGENT_LIST),
        # 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36',
        'Connection': 'close'
        }
    try:
        response = requests.get(url=url, headers=headers, timeout=3, proxies=proxies)
        if response.status_code == 200:
            response.encoding = 'utf-8'
            return response.text
        elif response.status_code in [500, 502, 503, 504]:
            logger.warning(f'{response.status_code}, 再一次请求')
            get(url=url)
        else:
            logger.error(f'请求 {url} 错误, {response.status_code}', exc_info=True)
            return
    except socket.timeout as e:
        logger.error(f'socket.timeout: {e.args}')
        return
    except ConnectTimeout as e:
        logger.error(f'ConnectTimeout: {e.args}')
        # get(url, proxies=proxies)
        return
    except ReadTimeout as e:
        logger.error(f'ReadTimeout: {e.args}')
        get(url, proxies=proxies)
    except ConnectionError as e:
        logger.error(f'ConnectionError: {e.args}')
        # get(url, proxies=proxies)
        return
    except RequestException as e:
        logger.error(f'请求 {url} 错误: {e.args}')
        return


def get_all_pages_urls(result_type, pages):
    """
    获取所有urls
    :param result_type: 成交公示类型
    :param pages: 当前类型的最大页码数
    :return: 当前类型的所有分页urls
    """
    if pages > 0:
        return [result_url + result_type + f'/index_{i}.htm' for i in range(2, pages)]
    else:
        return []